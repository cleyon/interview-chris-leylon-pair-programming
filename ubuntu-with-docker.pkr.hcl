# The AWS region might conceivably change, so make it a variable.
variable "aws_region" {
  type = string
  default = "us-east-1"
}

# The instance type could vary for each run, so make the user specify it
# on the command line, just because we can.
# Example:
#    packer build ubuntu-with-docker.pkr.hcl -var "instance_type=t2.micro"
variable "instance_type" {
  type = string
}


# Specify the source parameters of the bespoke AMI we want to build.
# Search for the most recent Ubuntu 16.04 server image available.
# 099720109477 is the account number for Canonical.  Set the AMI name,
# which will include a timestamp indicating the build time (encoded as
# epoch-seconds).  Packer will attempt to SSH into a temporary instance
# of this newly built AMI using the ssh_username specified.
source "amazon-ebs" "my-ubuntu" {
  ami_name = "My-Ubuntu-AMI.{{timestamp}}"
  instance_type = var.instance_type
  region = var.aws_region

  source_ami_filter {
    filters = {
      name = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
      root-device-type = "ebs"
      virtualization-type = "hvm"
    }
    owners = ["099720109477"]
    most_recent = true
  }
  ssh_username = "ubuntu"
}


# The build step builds the specified sources and invokes the
# provisioner.sh shell script, which upgrades packages to latest
# versions.
build {
  sources = ["source.amazon-ebs.my-ubuntu"]

  provisioner "shell" {
    scripts = fileset(".", "provisioner.sh")
  }
}
