#!/bin/bash

# Provision the nascent AMI by upgrading packages.
#
# Setting DEBIAN_FRONTEND to "noninteractive" is supposed to prevent apt
# from asking questions of the user.  We use it here because we're
# upgrading under the control of Packer, which does not provide an
# interactive environment, and *this particular* upgrade wants the user
# to answer questions about GRUB.
#
# NOTE: There are rumors of "high urgency" messages which ignore the
# DEBIAN_FRONTEND option.  In these cases, using the -q option (which is
# not recommended in general) is supposed to get around this.
# Fortunately we don't need to do it in our current case.

sudo DEBIAN_FRONTEND=noninteractive apt-get -y update
sudo DEBIAN_FRONTEND=noninteractive apt-get -y upgrade
